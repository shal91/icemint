//media queries
$huge: 1650px;
$desktop: 1200px;
$tablet: 992px;
$mobile: 768px;
@mixin respond-to($media) {
    @if $media == extrasmall {
        @media (max-width: $mobile - 1) {
            @content;
        }
    } @else if $media == mobile {
        @media (min-width: $mobile) and (max-width: $tablet - 1) {
            @content;
        }
    } @else if $media == tablet {
        @media (min-width: $tablet) and (max-width: $desktop - 1) {
            @content;
        }
    } @else if $media == desktop {
        @media (min-width: $desktop) and (max-width: $huge - 1) {
            @content;
        }
    } @else if $media == wide-screens {
        @media (min-width: $huge) {
            @content;
        }
    } @else if $media == less-wide-screens {
        @media (max-width: $huge - 1) {
            @content;
        }
    } @else if $media == less-desktop {
        @media (max-width: $desktop - 1) {
            @content;
        }
    } @else if $media == less-tablet {
        @media (max-width: $tablet - 1) {
            @content;
        }
    } @else if $media == more-desktop {
        @media (min-width: $desktop) {
            @content;
        }
    } @else if $media == more-tablet {
        @media (min-width: $tablet) {
            @content;
        }
    } @else if $media == more-mobile {
        @media (min-width: $mobile) {
            @content;
        }
    } @else if $media == more-extrasmall {
        @media (min-width: $mobile) {
            @content;
        }
    } @else if $media == mobile-tablet {
        @media (min-width: $mobile) and (max-width: $desktop - 1) {
            @content;
        }
    } @else if $media == mobile-desktop {
        @media (min-width: $mobile) and (max-width: $huge - 1) {
            @content;
        }
    }
}

@mixin near-devices($media) {
    @if $media == all {
        @media (min-width: $mobile) and (max-width: $mobile + 100) {
            @content;
        }
        @media (min-width: $tablet) and (max-width: $tablet + 100) {
            @content;
        }
        @media (min-width: $desktop) and (max-width: $desktop + 100) {
            @content;
        }
    } @else if $media == mobile-tablet {
        @media (min-width: $mobile) and (max-width: $mobile + 100) {
            @content;
        }
        @media (min-width: $tablet) and (max-width: $tablet + 100) {
            @content;
        }
    }

}

@mixin clearfix {
    &:after {
        content: "";
        display: table;
        clear: both;
        width: 100%;
    }
}

@mixin border-radius($data) {
    border-radius: $data;
    -webkit-border-radius: $data;
    -moz-border-radius: $data;
}

@mixin transition($data) {
    transition: unquote($data);
    -webkit-transition: unquote($data);
    -moz-transition: unquote($data);
}

@mixin placeholder() {
    &::-moz-placeholder {
        @content;
    }
    &::-webkit-input-placeholder {
        @content;
    }
}

@mixin vam {
    display: inline-block;
    vertical-align: middle;
}

@mixin text-hidden {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}

@mixin gradient($content) {
    background:-webkit-linear-gradient(unquote($content));
    background:-ms-linear-gradient(unquote($content));
    background:-moz-linear-gradient(unquote($content));
    background:linear-gradient(unquote($content));
}

@mixin transform($data) {
    transform: $data;
    -moz-transform: $data;
    -webkit-transform: $data;
    -o-transform: $data;
}

@mixin filter($filter-type,$filter-amount) {
    -webkit-filter: $filter-type+unquote('(#{$filter-amount})');
    -moz-filter: $filter-type+unquote('(#{$filter-amount})');
    -ms-filter: $filter-type+unquote('(#{$filter-amount})');
    -o-filter: $filter-type+unquote('(#{$filter-amount})');
    filter: $filter-type+unquote('(#{$filter-amount})');
}

@mixin cover {
    margin:auto;
    position: absolute;
    display: block;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
}