var gulp = require('gulp'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch');

gulp.task('default', function () {
    // place code for your default task here
});
gulp.task('sass', function () {
    return gulp.src(['./sass/main.scss'])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('css/'));
});

gulp.task('watch', function () {
    gulp.watch(['./sass/main.scss'],['sass']);
});
